program func
  use physics
  implicit none
  double precision :: integral
  real :: a, x,p, mass, velocity,b, h, diff_coeff, y_0, ODE
  integer :: i, n
  do i=1,15
     call random_number(mass)
     call random_number(velocity)
  end do
  a = 0
  b = 15 !bounds of our integral
  call trapezoidal_integral(f, a, b, n, integral)!CAlls the trapezoid rule from out physics module
  !print *, "Trapezoidal rule results: ", integral
  call simpsons_rule(f, a, b, n, integral)!Calls simpsons rule from out physics module
  !print *, "Simpsons composite 1/3 rule results: ", integral
  h = 1./5.
  y_0 = 4
  call eulers_method(y_prime, h, y_0, ODE)
  print *, ODE
  x = 3./2.
contains !integral 0-15 for sqrt(sin^3(x)+1)
   function f(x)
     implicit none
     real  :: x, f
     f = sqrt(cos(x)+x)
  end function f

  function y_prime(x,y)
    implicit none
    real :: x, y, y_prime
    y_prime = (3*x**2)*(2*sqrt(y))
  end function y_prime
  
end program func

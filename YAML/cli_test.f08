program cli
  ! riptutorial for cli in fortran
  implicit none
  character(len=100) :: num1char, num2char, num3char, num4char
  real :: num1, num2, num3, num4, numsum
  integer :: cli_num
  
  cli_num = 4

  ! make sure the right num of inputs have been provided
  if (COMMAND_ARGUMENT_COUNT().NE.cli_num) then
     
     write(*,*)'Eroor,',cli_num, ' command line arguments required, Stopping'
     STOP
  endif

  call get_command_argument(1, num1char)
  call get_command_argument(2, num2char)
  call get_command_argument(3, num3char)
  call get_command_argument(4, num4char)

  read(num1char,*)num1
  read(num2char,*)num2
  read(num3char,*)num3
  read(num4char,*)num4
  
  numsum = num1 + num2 + num3 + num4
  print*, "---"
  print*, numsum
  print*, "..."
end program cli
  

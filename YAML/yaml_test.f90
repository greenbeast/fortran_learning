! So at this point what I am thinking is it would be fun to add a CLI
! but also what I really need is to make this a module that can
! be called. I haven't done it yet because I know that I am still
! working stuff out and want to iron everything out beforehand.

! Instead of setting up this for cli I need to make a seperate script for cli

! Need to also add the - or should I just leave that to the user?

! Honestly, this whole ass thing needs to be refactored pretty badly and I need to rename variables.
! I did not expect this to get as convoluted as it did


! For some YAML formatting https://blog.codemagic.io/what-you-can-do-with-yaml/


! Could look into how to reading yaml as well

program yaml_export
      implicit none
      ! Use NEW_LINE('A') like \n
      integer :: word_length
      character(len=1000) :: word, word2, word3, inp1, output, yaml_dash_two_lines, yaml_dash_three_lines
      character(len=1000) :: inp2, inp3, inp4, yaml_add_line, yaml_dash_add_line, input
      character(len=1000) :: yaml_dash_four_lines, yaml_dash_five_lines
      word_length = 1000
      word = "This is some yaml output"
      word2 = "THis is word w"
      ! concat strings with //
      word3 = "  " // trim(word) // NEW_LINE('A') // "  " // word2

      inp1 = "Username: Hank"
      inp2 = "Password: MyPassword"
      inp3 = "Phone: 666299220"
      !inp3 = "Phone: 5419999999"//NEW_LINE('A')//"    "//"-hank@gdchillers.com"
      input = "Email: hank.greenburg@protonmail.com"
      
      inp4 = yaml_dash_three_lines(inp1, inp2, inp3, input) 
      output = yaml_dash_two_lines(inp1, inp2, inp3)
      ! Can't have an underscore in the name or it messes up the file name
      !call yaml_write_file("output.yaml", 12, inp4)
      print '(a)', "Three lines",NEW_LINE('A'),inp4
      print '(a)', "Two lines. ", NEW_LINE('A'),output
      
end program yaml_export

subroutine yaml_write_file(name, number, contents)
      implicit none
      character(len=20) :: name, dashes, dots
      character(len=100) :: contents
      integer :: number
      logical :: exists
      INQUIRE(FILE=name, EXIST=exists) ! checks to see if the file exists
!print *, exists
      dashes = "---"
      dots = "..."
      open(number, FILE=name)
! Need '(a)' to stop it from adding an extra space inthe beginning
!     of each line. Also, apparently this will make a new comment line
!     if you go long enough.
      write(number, '(a)') dashes

      write(number, '(a)') contents

      write(number, '(a)') dots
      
end subroutine yaml_write_file      



! This will compile all the lines and go one from there so they
! can all be in the same file

function yaml_add_line(input1, input2, input3) result(output)
      implicit none
      character(len=1000) :: input1, input2, input3, output1, output2, output, spc

      !print '(a)', input1 // NEW_LINE('A') // input2
      spc = "  "
      output1 = "  " // input1 // NEW_LINE('A') // "  " // input2
      !print *, output1
      output2 = output1 // NEW_LINE('A') // "  " // input3
      output = "  "//trim(input1)//NEW_LINE('A')//"  "//trim(input2)//NEW_LINE('A')//"  "//trim(input3)
      !print '(a)', input1 // NEW_LINE('A') // input2 // NEW_LINE('A') // input3
      !print *, output1
      !print '(a)', output

end function yaml_add_line


function yaml_dash_add_line(input1, input2, input3) result(output)
      implicit none
      character(len=1000) :: input1, input2, input3, output1, output2, output, spc

      !print '(a)', input1 // NEW_LINE('A') // input2
      spc = "  "
      output1 = "  " // input1 // NEW_LINE('A') // "  " // input2
      !print *, output1
      output2 = output1 // NEW_LINE('A') // "  " // input3
      output = "  "//trim(input1)//NEW_LINE('A')//"  "//trim(input2)//NEW_LINE('A')//"    -"//trim(input3)
      !print '(a)', input1 // NEW_LINE('A') // input2 // NEW_LINE('A') // input3
      !print *, output1
      !print '(a)', output

end function yaml_dash_add_line


! Maybe I can do something like this one below but have it be unlimited arguments
! so then that way I can get more dashed outputs. Also though I can name them 
! dassh_one...dash_n and go that route?

function yaml_dash_two_lines(input1, input2, input3) result(output)
      implicit none
      character(len=1000) :: input1, input2, input3, output
      output = "  "//trim(input1)//NEW_LINE('A')//"    -"//trim(input2)//NEW_LINE('A')//"    -"//trim(input3)

end function yaml_dash_two_lines


function yaml_dash_three_lines(input1, input2, input3, input4) result(output)
      implicit none
      character(len=1000) :: input1, input2, input3, input4, output
      ! Use the & because if you go over line width 132 it'll throw an error
      output = "  "//trim(input1)//NEW_LINE('A')//"    -"//trim(input2)//NEW_LINE('A')&
           //"    -"//trim(input3)//NEW_LINE('A')//"    -"//trim(input4)

end function yaml_dash_three_lines


function yaml_dash_four_lines(input1, input2, input3, input4, input5) result(output)
      implicit none
      character(len=1000) :: input1, input2, input3, input4, input5, output
      output = "  "//trim(input1)//NEW_LINE('A')//"    -"//trim(input2)//&
           NEW_LINE('A')//"    -"//trim(input3)//NEW_LINE('A')//"    -"//trim(input4)&
           //NEW_LINE('A')//"    -"//trim(input5)

end function yaml_dash_four_lines



function yaml_dash_five_lines(input1, input2, input3, input4, input5, input6) result(output)
  implicit none
  character(len=1000) :: input1, input2, input3, input4, input5, input6, output
  output = "  "//trim(input1)//NEW_LINE('A')//"    -"//trim(input2)//&
           NEW_LINE('A')//"    -"//trim(input3)//NEW_LINE('A')//"    -"//trim(input4)&
           //NEW_LINE('A')//"    -"//trim(input5)//NEW_LINE('A')//"    -"//trim(input6)


end function yaml_dash_five_lines




! can't get this to work



!function yaml_add_indent() result(indent)
!  implicit none
!  character(len=4) :: indent
!  indent = "  "
!
!
!end function yaml_add_indent

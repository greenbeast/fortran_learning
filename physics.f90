!When compiling modules have to use -c with gfortran like 'gfortran -c physics.f90'
!Need to add a bit more helpful comments here
module physics
  implicit none
contains

  subroutine momentum(mass, velocity, p)!p=m*v
    real, intent(in) :: mass, velocity
    real :: p
    p = mass*velocity!momentum is p
  end subroutine momentum

  subroutine force(mass, acceleration)!f=ma
    real, intent(in) :: mass, acceleration
    real :: f
    f = mass*acceleration!f=ma
  end subroutine force

  subroutine mass_acceleration(force, mass)
    real, intent(in) :: force, mass
    real :: accel
    accel = force/mass
  end subroutine mass_acceleration

  subroutine vel_acceleration(dv,dt)
    real, intent(in) :: dv, dt
    real :: accel
    accel = dv/dt!Change in velocity times change in time
  end subroutine vel_acceleration

  subroutine trapezoidal_integral(f, a, b, n, integral)!f has to be a function that we are integrating
    !((b-a)/n)/2 (f(x_0)+2(f(x_1))...+...2(f(x_n)))
    double precision :: integral, dx, test
    real, intent(in) :: a, b
    real :: f, dh
    integer :: n
    integral = 0.0
    dx = ((b-a)/10)!n is 10 in this routine
    do n=0,10!Update to 10 trapezoids mostly because I don't want to write out more
       dh = n*dx !This is the gaps between a and b
       !print *,'dh: ', dh
       if (n==0) then
          integral = f(dh)
       elseif (n>0 .and. n<10) then
          integral = integral + 2*f(dh)
       elseif (n==10) then
          integral = integral + f(dh)
       end if
       
    end do
    integral = dx/2*integral
  end subroutine trapezoidal_integral

  subroutine simpsons_rule(f, a, b, n, integral)!Composit Simpsons 1/3 rule if value is odd multiplies by 2 and if even multiplies by 4
    !This is so that it makes up for the fact that a parabola is a bit hard to get integrals that aren't smooth or are highly occilating
    double precision :: integral, dx
    real, intent(in) :: a, b
    real :: f, dh
    integer :: n
    dx = ((b-a)/10)
    integral = 0.0
    do n=0,10
       dh = n*dx
       if (n==0) then
          integral = f(a)
       elseif (mod(n,2)==0 .and. n<10) then !This means it is even
          integral = integral + 2*f(dh)
       elseif (mod(n,2) /= 0) then !means it is odd
          integral = integral +4*f(dh)
       elseif (n==10) then
          integral = integral + f(dh)
       end if
    end do
    integral = dx/3*integral
  end subroutine simpsons_rule

  subroutine kinetic_energy(mass, velocity, energy)
    implicit none
    real, intent(in) :: mass, velocity
    real :: energy
    energy = (1./2.)*mass*velocity**2
  end subroutine kinetic_energy

  subroutine potential_energy(mass, height, energy)!PE=mgh
    implicit none
    real, intent(in) :: mass, height
    real :: g, energy
    g = 9.82!Gravity
    energy = mass*g*height
  end subroutine potential_energy

  subroutine work(force, distance, angle, energy)
    implicit none
    real, intent(in) :: force, distance, angle
    real :: energy
    energy = force*distance*cos(angle)
  end subroutine work

  subroutine grav_force(m1,m2, radius, force)
    implicit none
    real, intent(in) :: m1, m2, radius
    real :: force, grav_const
    grav_const = 6.67*10**(-11)
    force = -((grav_const*m1*m2)/(radius**2))    
  end subroutine grav_force

  subroutine torque(radius, force, angle, torq)
    implicit none
    real, intent(in) :: radius, force, angle
    real :: torq
    torq = radius*force*sin(angle)
  end subroutine torque

  subroutine em_force(q1,q2,radius,force)
    implicit none
    real, intent(in) :: q1, q2, radius
    real :: pi, epsilon, k, force
    pi = 4*atan(1.0)
    epsilon = 8.85*10**(-12)
    k = 1/(4*pi*epsilon)
    force = k*((q1*q2)/radius**2)
  end subroutine em_force

  subroutine electric_field(force, q, energy)
    implicit none
    real, intent(in) :: force, q
    real :: energy
    energy = force/q
  end subroutine electric_field

  subroutine potential_electric_energy(q1,q2,radius,force)
    implicit none
    real, intent(in) :: q1, q2, radius
    real :: force, pi, epsilon, k
    pi = 4*atan(1.0)
    epsilon = 8.85*10**(-12)
    k = 1/(4*pi*epsilon)
    force = k*((q1*q2)/radius)
  end subroutine potential_electric_energy

  subroutine current(dq, dt, avg_current)
    implicit none
    real, intent(in) :: dq, dt
    real :: avg_current
    avg_current = dq/dt    
  end subroutine current

  subroutine magnetic_field(current, radius, field)
    implicit none
    real, intent(in) :: current, radius
    real :: field, pi, mu
    pi = 4*atan(1.0)
    mu = 4*pi*10**(-7)!permeability of free space
    field = (mu*current)/(2*pi*radius)
  end subroutine magnetic_field

  subroutine emf(magnetic_field, length, velocity, emf_output)
    implicit none
    real, intent(in) :: magnetic_field, length, velocity
    real :: emf_output
    emf_output = magnetic_field*length*velocity
  end subroutine emf

  !Difference quotent
  subroutine diff_quotent(f, x, h, derivative)
    implicit none
    real, intent(in) :: x, h
    real :: f, derivative
    derivative = ((f(x+h)-f(x)))/h
    
  end subroutine diff_quotent
  
  !ODE's
  subroutine eulers_method(y_prime, h, y_0, ODE)
    implicit none
    real, intent(in) :: h, y_0
    real :: y_prime, ODE, t_n, steps, y_n
    integer :: i
    steps = 1/h
    t_n = 0
    y_n = y_0
    do i=1,steps
       !t_n = t_n + h
       ODE = y_n + h * y_prime(t_n,y_n)
       t_n = t_n + h
       y_n = ODE
    end do
    !print *, ODE
  end subroutine eulers_method
  
end module physics

program sphere
  use values!Calling my values module
  implicit none
  double precision :: rad, vol, surface
  rad = 12
  call surface_area2(rad, surface)
  print *, 'Surface area: ', surface

  call volume(rad, vol)
  print *, 'Volume: ',vol
  
end program sphere


subroutine volume2(rad, vol)
  implicit none
  double precision ::rad, vol, pi
  pi = 4*atan(1.0)
  vol = 4./3.*pi*rad**3

end subroutine volume2

subroutine surface_area2(rad, surface)
  implicit none
  double precision :: rad
  double precision :: pi, surface
  pi = 4*atan(1.0)
  surface = 4.0*pi*rad**2

end subroutine surface_area2

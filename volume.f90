program vols
  implicit none
  !Calculate different between volume of two spheres
  double precision :: rad1, rad2, vol1, vol2

  print *, 'Please eneter radius 1 and radius 2'
  read *, rad1, rad2
  !Calculate volume
  call volume(rad1, vol1)
  call volume(rad2, vol2)
  print *, 'Difference in volume is ', abs(vol1-vol2)
end program vols
!----------------------------------
subroutine volume(rad,vol)
  implicit none
  double precision :: rad, vol, pi
  pi = 4*atan(1.0)
  
  vol = 4./3.*pi*rad**3
  
end subroutine volume
